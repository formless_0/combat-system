// --------- CARDS ---------

function Card(type, title, img, color, damage, speed, cost, defense) {
	
	this.type = type;
	this.title = title;
	this.img = img;
	this.color = color;
	
	this.damage = damage;
	this.speed = speed;
	this.cost = cost;
	this.defense = defense;
	
}

var atk0 = new Card(0,"Light Attack","atk0","blue-grey lighten-3",1,4,1,null);
var atk1 = new Card(0,"Medium Attack","atk1","blue-grey lighten-2",2,3,2,null);
var atk2 = new Card(0,"Heavy Attack","atk2","blue-grey lighten-1",3,2,3,null);
var def0 = new Card(1,"Light Defense","def0","brown lighten-3",null,null,null,1);
var def1 = new Card(1,"Medium Defense","def1","brown lighten-2",null,null,null,2);
var def2 = new Card(1,"Heavy Defense","def2","brown lighten-1",null,null,null,3);


var defaultDeck = [
	atk0,
	atk1,
	atk2,
	def0,
	def1,
	def2
];


// --------- INITIALIZE ---------

var LIFE = 5;
var FULL = 5;
var DEFENSE = 5;
var DAMAGE;
var SPEED;
var COST;

var playerDamage;
var cpuDamage;

var playerSpeed;
var cpuSpeed;

var playerCost;
var cpuCost;

var playerDefense;
var cpuDefense;

var playerHealth;
var cpuHealth;

var playerStamina;
var cpuStamina;

var playerPreviousAction;
var cpuPreviousAction;

var playerDefensePoints;
var cpuDefensePoints;

var playerLightCooldown;
var cpuLightCooldown;

var playerHeavyCooldown;
var cpuHeavyCooldown;


var playerDeck;

var turn;
var i;

function initialize() {

	playerHealth = 	LIFE;
	cpuHealth = 	LIFE;
	playerStamina = FULL;
	cpuStamina = 	FULL;
	playerDefensePoints = DEFENSE;
	cpuDefensePoints = 	DEFENSE;
	
	playerDeck = defaultDeck;
	
	playerPreviousAction = 1;
	cpuPreviousAction = 1;
	
	playerLightCooldown = 0;
	cpuLightCooldown = 0;
	playerHeavyCooldown = 0;
	cpuHeavyCooldown = 0;
	
	turn = 0;
	
	document.getElementById("turn").innerHTML = "Round: "+turn;
	
	document.getElementById("playerStamina").innerHTML = "Stamina: "+playerStamina;
	document.getElementById("playerDefense").innerHTML = "Defense: "+playerDefensePoints;
	document.getElementById("playerHealth").innerHTML = "Health: "+playerHealth;
	document.getElementById("playerAction").innerHTML = "Action: none";

	document.getElementById("cpuStamina").innerHTML = "Stamina: "+cpuStamina;
	document.getElementById("cpuDefense").innerHTML = "Defense: "+cpuDefensePoints;
	document.getElementById("cpuHealth").innerHTML = "Health: "+cpuHealth;
	document.getElementById("cpuAction").innerHTML = "Action: none";
	
	for(i=0; i<playerDeck.length; i++)
		document.getElementById("card"+i).innerHTML = cardPrint(playerDeck[i]);
		
	document.getElementById("card0").style.filter = "brightness(100%)";
	document.getElementById("card2").style.filter = "brightness(100%)";
	
}


// --------- CARDS PRINT ---------

function cardPrint(card) {
	
	var cardContent;
	
	// card.type=0:	attack
	// card.type=1:	defense
	
	if(card.type==0) {
		cardContent = 	"<p>Damage: "+card.damage+"</p>"+
						"<p>Speed: "+card.speed+"</p>"+
						"<p>Cost: "+card.cost+"</p>";
	} else {
		cardContent = 	"<p>Defense: "+card.defense+"</p>";
	}
	
	return	"<div onclick=\"action(playerDeck["+i+"])\" class=\"card hoverable "+card.color+"\">"+
				"<div class=\"card-image\">"+
					"<img src=\"images/"+card.img+".png\">"+
				"</div>"+
				"<div class=\"card-content\">"+
				"<span class=\"card-title\">"+card.title+"</span>"+cardContent+"</div>"+
			"</div>";
}


// --------- ACTION PHASE ---------

function action(playerCard) {
	
	var cpuAction;
	var nextTurn = true;
	
	playerDamage = 0;
	playerSpeed = 0;
	playerCost = 0;
	playerDefense = 0;
	
	if(playerCard) {
		
		if(playerCard.type == 0) {
			playerAction = 0;
			playerDamage = playerCard.damage;
			playerSpeed = playerCard.speed;
			playerCost = playerCard.cost;
		} else {
			playerAction = 1;
			playerDefense = playerCard.defense;
		}
		
	} else {
		playerAction = 2;
	}
	
	do {
		cpuAction = randomGenerator(0, 2);
		cpuDamage = 0;
		cpuSpeed = 0;
		cpuDefense = 0;
		cpuCost = 0;
		
		if(cpuAction == 0) {
			
			switch(randomGenerator(0,2)) {
				
				case 0: {
					cpuDamage = 1;
					cpuSpeed = 4;
					cpuCost = 1;
				}
				break;
				
				case 1: {
					cpuDamage = 2;
					cpuSpeed = 3;
					cpuCost = 2;
				}
				break;
				
				case 2: {
					cpuDamage = 3;
					cpuSpeed = 2;
					cpuCost = 3;
				}	
				break;
			}
		}
		
		if(cpuAction == 1) {
			
			switch(randomGenerator(0,2)) {
				
				case 0: {
					cpuDefense = 1;
				}
				break;
				
				case 1: {
					cpuDefense = 2;
				}
				break;
				
				case 2: {
					cpuDefense = 3;
				}	
				break;
			}
		}

	} while ((cpuCost>cpuStamina) || (cpuAction==2 && cpuStamina>3) || (cpuAction==1 && cpuDefensePoints==0) || (cpuDefense>cpuDefensePoints));
	
	if(playerPreviousAction != 1 && playerDefensePoints<DEFENSE) playerDefensePoints++;
	if(cpuPreviousAction != 1 && cpuDefensePoints<DEFENSE) cpuDefensePoints++;
	
	switch(playerAction) {

		case 0: {
			
			if(playerCost > playerStamina) {
				alert("You haven't enough stamina for that attack!");
				nextTurn = false;
				break;
			}
			
			if(playerDamage == 1) {
				if(playerLightCooldown != 0) {
					alert("You can't use the Light Attack yet!");
					nextTurn = false;
					break;
				} else {
					playerLightCooldown = 3; // actually is 2
				}
			}
			
			if(playerDamage == 3) {
				if(playerHeavyCooldown != 0) {
					alert("You can't use the Heavy Attack yet!");
					nextTurn = false;
					break;
				} else {
					playerHeavyCooldown = 3; // actually is 2
				}
			}
			
			switch(cpuAction) {
				case 0: {
					
					playerStamina -= playerCost;
					cpuStamina -= cpuCost;
					
					if(playerSpeed-cpuSpeed>0) {
						cpuHealth -= playerDamage;
						break;
					}
					
					if(playerSpeed-cpuSpeed<0) {
						playerHealth -= cpuDamage;
						break;
					}
					
					playerHealth -= cpuDamage;
					cpuHealth -= playerDamage;
					
				}
				break;
				
				case 1: {
					cpuDefensePoints -= cpuDefense;
					playerHealth -= 0;
					playerStamina -= playerCost;
					if(playerDamage>cpuDefense) cpuHealth -= playerDamage; else cpuHealth -= 0;
					cpuStamina -= 0;
				}
				break;
				
				case 2: {
					playerHealth -= 0;
					playerStamina -= playerCost;
					cpuHealth -= playerDamage;
					cpuStamina = FULL;
				}
			}
		}
		break;
		
		case 1: {
			
			if(playerDefense > playerDefensePoints) {
				alert("You haven't enough defense points!");
				nextTurn = false;
				break;
			}

			playerDefensePoints -= playerDefense;			

			switch(cpuAction) {
				case 0: {
					if(cpuDamage>playerDefense) playerHealth -= cpuDamage; else playerHealth -= 0;
					playerStamina -= 0;
					cpuHealth -= 0;
					cpuStamina -= cpuCost;
				}
				break;
				case 1: {
					cpuDefensePoints -= cpuDefense;
					playerHealth -= 0;
					playerStamina -= 0;
					cpuHealth -= 0;
					cpuStamina -= 0;
				}
				break;
				case 2: {
					playerHealth -= 0;
					playerStamina -= 0;
					cpuHealth -= 0;
					cpuStamina = FULL;
				}
			}
		}
		break;
		
		case 2: {

			if(playerStamina == 5) {
				alert("You're stamina is already full!");
				nextTurn = false;
				break;
			}			

			switch(cpuAction) {
				case 0: {
					playerHealth -= cpuDamage;
					playerStamina = FULL;
					cpuHealth -= 0;
					cpuStamina -= cpuCost;
				}
				break;
				case 1: {
					cpuDefensePoints -= cpuDefense;
					playerHealth -= 0;
					playerStamina = FULL;
					cpuHealth -= 0;
					cpuStamina -= 0;
				}
				break;
				case 2: {
					playerHealth -= 0;
					playerStamina = FULL;
					cpuHealth -= 0;
					cpuStamina = FULL;
				}
			}
		}
	}
	
	if(nextTurn) {
		playerPreviousAction = playerAction;
		cpuPreviousAction = cpuAction;
		turn++;
		
		if(playerLightCooldown>0) {
			playerLightCooldown--;
			switch(playerLightCooldown) {
				case 2: document.getElementById("card0").style.filter = "brightness(40%)"; break;
				case 1: document.getElementById("card0").style.filter = "brightness(60%)"; break;
				case 0: document.getElementById("card0").style.filter = "brightness(100%)";
			}
		}
		
		if(playerHeavyCooldown>0) {
			playerHeavyCooldown--;
			switch(playerHeavyCooldown) {
				case 2: document.getElementById("card2").style.filter = "brightness(40%)"; break;
				case 1: document.getElementById("card2").style.filter = "brightness(60%)"; break;
				case 0: document.getElementById("card2").style.filter = "brightness(100%)";
			}
		}

		
		if(playerHealth<0) 	playerHealth = 0;
		if(cpuHealth<0) 	cpuHealth = 0;
	
		document.getElementById("turn").innerHTML = "Round: "+turn;
	
		document.getElementById("playerStamina").innerHTML = "Stamina: "+playerStamina;
		document.getElementById("playerDefense").innerHTML = "Defense: "+playerDefensePoints;
		document.getElementById("playerHealth").innerHTML = "Health: "+playerHealth;
	
		document.getElementById("cpuStamina").innerHTML = "Stamina: "+cpuStamina;
		document.getElementById("cpuDefense").innerHTML = "Defense: "+cpuDefensePoints;
		document.getElementById("cpuHealth").innerHTML = "Health: "+cpuHealth;
	
		switch(playerAction) {	
			case 0: document.getElementById("playerAction").innerHTML = "Action: attack "+playerDamage; break;
			case 1: document.getElementById("playerAction").innerHTML = "Action: defense "+playerDefense; break;
			case 2: document.getElementById("playerAction").innerHTML = "Action: recover";
		}
	
		switch(cpuAction) {
			case 0: document.getElementById("cpuAction").innerHTML = "Action: attack "+cpuDamage; break;
			case 1: document.getElementById("cpuAction").innerHTML = "Action: defense "+cpuDefense; break;
			case 2: document.getElementById("cpuAction").innerHTML = "Action: recover"; 
		}
	}
	
	if(playerHealth <= 0 && cpuHealth <= 0) {
		alert("Draw");
		initialize();
	}
	else {
		if(playerHealth <= 0) {
			alert("You Lose!");
			initialize();
		}
	
		if(cpuHealth <= 0) {
			alert("You Win!");
			initialize();
		}
	}
}


// --------- RANDOM GENERATOR ---------

function randomGenerator(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}