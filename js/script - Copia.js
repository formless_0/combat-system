// --------- CARDS ---------

function Card(type, damage, speed, cost, defense) {
	this.type = type;
	this.damage = damage;
	this.speed = speed;
	this.cost = cost;
	this.defense = defense;
}

var atk0 = new Card(0,1,4,1,null);
var atk1 = new Card(0,2,3,2,null);
var atk2 = new Card(0,3,2,3,null);
var atk3 = new Card(0,4,1,4,null);
var def0 = new Card(1,null,null,null,1);
var def1 = new Card(1,null,null,null,2);
var def2 = new Card(1,null,null,null,3);
var def3 = new Card(1,null,null,null,4);


var defaultDeck = [
	atk0,
	atk0,
	atk0,
	atk1,
	atk1,
	atk1,
	atk2,
	atk2,
	atk2,
	atk3,
	atk3,
	atk3,
	def0,
	def0,
	def0,
	def1,
	def1,
	def1,
	def2,
	def2,
	def2,
	def3,
	def3,
	def3
];


// --------- INITIALIZE ---------

var LIFE = 5;
var FULL = 5;
var DEFENSE = 3;
var DAMAGE;
var SPEED;
var COST;

var playerDamage;
var cpuDamage;

var playerSpeed;
var cpuSpeed;

var playerCost;
var cpuCost;

var playerDefense;
var cpuDefense;

var playerHealth;
var cpuHealth;

var playerStamina;
var cpuStamina;

var playerPreviousAction;
var cpuPreviousAction;

var playerDefensePoints;
var cpuDefensePoints;

var playerDeck;

var turn;

function initialize() {

	playerHealth = 	LIFE;
	cpuHealth = 	LIFE;
	playerStamina = FULL;
	cpuStamina = 	FULL;
	playerDefensePoints = DEFENSE;
	cpuDefensePoints = 	DEFENSE;
	
	playerDeck = defaultDeck;
	
	playerPreviousAction = 1;
	cpuPreviousAction = 1;
	turn = 0;
	
	document.getElementById("turn").innerHTML = "Round: "+turn;
	
	document.getElementById("playerStamina").innerHTML = "Stamina: "+playerStamina;
	document.getElementById("playerDefense").innerHTML = "Defense: "+playerDefensePoints;
	document.getElementById("playerHealth").innerHTML = "Health: "+playerHealth;
	document.getElementById("playerAction").innerHTML = "Action: none";

	document.getElementById("cpuStamina").innerHTML = "Stamina: "+cpuStamina;
	document.getElementById("cpuDefense").innerHTML = "Defense: "+cpuDefensePoints;
	document.getElementById("cpuHealth").innerHTML = "Health: "+cpuHealth;
	document.getElementById("cpuAction").innerHTML = "Action: none";
	
	shuffle(playerDeck);
}

// --------- SHUFFLE ---------

function shuffle(deck) {
	
	var m = deck.length;
	var t;
	var	j;
	var debug = "";

	for(var k=0; k<deck.length; k++) {
		debug+= deck[k].type+" ";
	}
	debug += "<br>";
		
	// While there remain elements to shuffle…
	while (m) {
		// Pick a remaining element…
		j = Math.floor(Math.random() * m--);

		// And swap it with the current element.
		t = deck[m];
		deck[m] = deck[j];
		deck[j] = t;
	}

	for(k=0; k<deck.length; k++) {
		debug+= deck[k].type+" ";
	}

	document.getElementById("debug").innerHTML = debug;
	drawACard(deck);
	
}


// --------- CARDS PRINT ---------

var i = 0;
var numberOfCards = 0; //numero di carte in mano (deve essere sempre uguale a 4)
var handPrint = ""; //mostra le carte in mano

function drawACard(deck) {

	while(numberOfCards<4) {
		handPrint += "<div id=\"pos"+i+"\" class=\"col s12 m3\">"+cardPrint(deck, deck[i].type)+"</div>";
		i++;
		numberOfCards++;
	}		
	
	document.getElementById("cardsContainer").innerHTML = handPrint;

}

function cardPrint(deck, cardType) {
	
	var cardColor;
	var cardContent;
	
	// cardType=0:	attack
	// cardType=1:	defense
	
	if(cardType==0) {
		cardColor = 	"blue-grey lighten-4";
		cardContent = 	"<p>Damage: "+deck[i].damage+"</p>"+
						"<p>Speed: "+deck[i].speed+"</p>"+
						"<p>Cost: "+deck[i].cost+"</p>";
	} else {
		cardColor = 	"brown lighten-1";
		cardContent = 	"<p>Defense: "+deck[i].defense+"</p>";
	}
	
	return	"<div class=\"card "+cardColor+"\">"+
				"<div class=\"card-content\">"+cardContent+"</div>"+
				"<div class=\"card-action\">"+
					"<a class=\"waves-effect waves-teal btn-flat\" onclick=\"action(playerDeck["+i+"],"+i+")\">USE</a>"+
				"</div>"+
			"</div>";
}


// --------- ACTION PHASE ---------

function action(playerCard, pos) {
	
	var cpuAction;
	var nextTurn = true;
	var removeCard = false;
	
	if(playerCard) {
		
		if(playerCard.type == 0) {
			playerAction = 0;
			playerDamage = playerCard.damage;
			playerSpeed = playerCard.speed;
			playerCost = playerCard.cost;
		} else {
			playerAction = 1;
			playerDefense = playerCard.defense;
		}
		
		removeCard = true;
		
	} else {
		playerAction = 2;
	}
	
	do {
		cpuAction = randomGenerator(0, 2);
		
		if(cpuAction != 2 && cpuStamina>0) {
			
			switch(randomGenerator(0,3)) {
				
				case 0: {
					cpuDamage = 1;
					cpuSpeed = 4;
					cpuCost = 1;
					cpuDefense = 1;
				}
				break;
				
				case 1: {
					cpuDamage = 2;
					cpuSpeed = 3;
					cpuCost = 2;
					cpuDefense = 2;
				}
				break;
				
				case 2: {
					cpuDamage = 3;
					cpuSpeed = 2;
					cpuCost = 3;
					cpuDefense = 2;
				}	
				break;

				case 3: {
					cpuDamage = 4;
					cpuSpeed = 1;
					cpuCost = 4;
					cpuDefense = 4;
				}
			}
		}

	} while ((cpuCost>cpuStamina) || (cpuAction==2 && cpuStamina>2) || (cpuAction==1 && cpuDefensePoints==0));
	
	if(playerPreviousAction != 1 && playerDefensePoints<DEFENSE) playerDefensePoints++;
	if(cpuPreviousAction != 1 && cpuDefensePoints<DEFENSE) cpuDefensePoints++;
	
	switch(playerAction) {

		case 0: {
			
			if(playerCost > playerStamina) {
				alert("You haven't enough stamina for that attack!");
				nextTurn = false;
				break;
			}
			
			switch(cpuAction) {
				case 0: {
					if(cpuDamage>playerHealth) playerHealth=0; else playerHealth-= cpuDamage;
					playerStamina -= playerCost;
					if(playerDamage>cpuHealth) cpuHealth=0; else cpuHealth-= playerDamage;
					cpuStamina -= cpuCost;
				}
				break;
				
				case 1: {
					cpuDefensePoints--;
					playerHealth -= 0;
					playerStamina -= playerCost;
					cpuHealth -= 0;
					cpuStamina -= 0;
				}
				break;
				
				case 2: {
					playerHealth -= 0;
					playerStamina -= playerCost;
					if(playerDamage>cpuHealth) cpuHealth=0; else cpuHealth-= playerDamage;
					cpuStamina = FULL;
				}
			}
		}
		break;
		
		case 1: {
			
			if(playerDefensePoints== 0) {
				alert("You haven't enough defense points!");
				nextTurn = false;
				break;
			}

			playerDefensePoints--;			

			switch(cpuAction) {
				case 0: {
					playerHealth -= 0;
					playerStamina -= 0;
					cpuHealth -= 0;
					cpuStamina -= cpuCost;
				}
				break;
				case 1: {
					cpuDefensePoints--;
					playerHealth -= 0;
					playerStamina -= 0;
					cpuHealth -= 0;
					cpuStamina -= 0;
				}
				break;
				case 2: {
					playerHealth -= 0;
					playerStamina -= 0;
					cpuHealth -= 0;
					cpuStamina = FULL;
				}
			}
		}
		break;
		
		case 2: {

			if(playerStamina == 5) {
				alert("You're stamina is already full!");
				nextTurn = false;
				break;
			}			

			switch(cpuAction) {
				case 0: {
					if(cpuDamage>playerHealth) playerHealth=0; else playerHealth-= cpuDamage;
					playerStamina = FULL;
					cpuHealth -= 0;
					cpuStamina -= cpuCost;
				}
				break;
				case 1: {
					cpuDefensePoints--;
					playerHealth -= 0;
					playerStamina = FULL;
					cpuHealth -= 0;
					cpuStamina -= 0;
				}
				break;
				case 2: {
					playerHealth -= 0;
					playerStamina = FULL;
					cpuHealth -= 0;
					cpuStamina = FULL;
				}
			}
		}
	}
	
	if(nextTurn) {
		playerPreviousAction = playerAction;
		cpuPreviousAction = cpuAction;
		turn++;
	
		document.getElementById("turn").innerHTML = "Round: "+turn;
	
		document.getElementById("playerStamina").innerHTML = "Stamina: "+playerStamina;
		document.getElementById("playerDefense").innerHTML = "Defense: "+playerDefensePoints;
		document.getElementById("playerHealth").innerHTML = "Health: "+playerHealth;
	
		document.getElementById("cpuStamina").innerHTML = "Stamina: "+cpuStamina;
		document.getElementById("cpuDefense").innerHTML = "Defense: "+cpuDefensePoints;
		document.getElementById("cpuHealth").innerHTML = "Health: "+cpuHealth;
	
		switch(playerAction) {	
			case 0: document.getElementById("playerAction").innerHTML = "Action: attack "+playerDamage; break;
			case 1: document.getElementById("playerAction").innerHTML = "Action: defense "+playerDefense; break;
			case 2: document.getElementById("playerAction").innerHTML = "Action: recover";
		}
	
		switch(cpuAction) {
			case 0: document.getElementById("cpuAction").innerHTML = "Action: attack "+cpuDamage; break;
			case 1: document.getElementById("cpuAction").innerHTML = "Action: defense "+cpuDefense; break;
			case 2: document.getElementById("cpuAction").innerHTML = "Action: recover"; 
		}
		
		if(removeCard) {
			numberOfCards--;
			document.getElementById("pos"+pos).innerHTML = drawACard(playerDeck);
		}
	}
	
	if(playerHealth == 0 && cpuHealth == 0) {
		alert("Draw");
		initialize();
	}
	else {
		if(playerHealth == 0) {
			alert("You Lose!");
			initialize();
		}
	
		if(cpuHealth == 0) {
			alert("You Win!");
			initialize();
		}
	}
}


// --------- RANDOM GENERATOR ---------

function randomGenerator(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}